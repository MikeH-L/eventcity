﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyByCameraCinematic : MonoBehaviour
{
    public Animation _cameraAnimation;
    public GameObject[] _gameobjectsToHide;
    public GameObject _stopAnimationButton;
    //private Transform _startPositionCamera;
    private float _lengthOfClip = 0f;
    private float counter = 0f;

    private void Start()
    {
        _lengthOfClip = _cameraAnimation.clip.length;
       // _startPositionCamera = _cameraAnimation.GetComponent<Transform>();
    }

    [ContextMenu("FlyByCameraTest")]
    public void TriggerFlyBySmoothCameraAnimation()
    {
        _stopAnimationButton.SetActive(true);

        if (gameObject.GetComponent<ToggleLights>()._lightsOn == false)
        {
        gameObject.GetComponent<ToggleLights>().TurnOnLights();
        }

        _cameraAnimation.enabled = true;
        _cameraAnimation.Play();
        foreach (GameObject go in _gameobjectsToHide)
        {
            go.SetActive(false);
        }

        StartCoroutine(WaitForAnimation(_cameraAnimation.clip.length));

        if (counter >= _lengthOfClip)
        {
            foreach (GameObject go in _gameobjectsToHide)
            {
                go.SetActive(true);
            }
            counter = 1;
        }
    }


    public IEnumerator WaitForAnimation(float animationLength)
    {
        float counter = 0f;

        while (counter < animationLength)
        {

            // If Player press Esc THEN the Position of the camera gets the value from the last position of the "cameraAngles" Array which is the startposition of all camera angles.
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                counter = animationLength;
                _cameraAnimation.Stop();
                  _cameraAnimation.GetComponent<Transform>().position = gameObject.GetComponent<ChangeCameraAngle>()._cameraAngles[gameObject.GetComponent<ChangeCameraAngle>()._cameraAngles.Length - 1].position;
                   _cameraAnimation.GetComponent<Transform>().rotation = gameObject.GetComponent<ChangeCameraAngle>()._cameraAngles[gameObject.GetComponent<ChangeCameraAngle>()._cameraAngles.Length - 1].rotation;
               // _cameraAnimation.GetComponent<Transform>().position = _startPositionCamera.position;
              //  _cameraAnimation.GetComponent<Transform>().rotation = _startPositionCamera.rotation;
                _stopAnimationButton.SetActive(false);
            }
            counter += Time.deltaTime;
            yield return 0;
        }

        if (counter >= animationLength)
        {
            foreach (GameObject go in _gameobjectsToHide)
            {
                go.SetActive(true);
                _stopAnimationButton.SetActive(false);
            }
        }
    }
}
