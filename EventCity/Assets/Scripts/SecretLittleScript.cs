﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SecretLittleScript : MonoBehaviour
{
    public GameObject[] _prefabsCarsParent;
    public GameObject[] _hideGameObjects;
    public Button TriggerButton;
    private Rigidbody[] _allchildrenRigidBodys;
    private Transform[] _allchildrenTransforms;
    private List<Vector3> _gameObjectStartsPosition = new List<Vector3>();
    private List<Quaternion> _gameObjectStartsRotation = new List<Quaternion>();

    public AudioSource _mayhemSoundource;
    private float _lerpTime = 4.5f;
    private float _speedTime = 0.02f;

    private void Awake()
    {
        foreach (var item in _prefabsCarsParent)
        {
        _allchildrenTransforms = item.GetComponentsInChildren<Transform>();
        }

        foreach (Transform item in _allchildrenTransforms)
        {
            item.gameObject.AddComponent<Rigidbody>();
            item.gameObject.GetComponent<Rigidbody>().useGravity = false;
            item.gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
        foreach (GameObject item in _prefabsCarsParent)
        {
        _allchildrenRigidBodys = item.GetComponentsInChildren<Rigidbody>();
        }

        foreach (Transform go in _allchildrenTransforms)
        {
            _gameObjectStartsPosition.Add(go.position);
            _gameObjectStartsRotation.Add(go.rotation);
        }
    }


    public void BackToStartValues()
    {
        TriggerButton.interactable = true;
        _mayhemSoundource.Stop();

        foreach (Rigidbody rigidbodys in _allchildrenRigidBodys)
        {
            rigidbodys.isKinematic = true;
            rigidbodys.useGravity = false;
        }
        gameObject.GetComponent<CarMayhem>().BackToStartValues();
        StartCoroutine(LerpBackToStartPositionCoroutine());

        Invoke("ShowAllGameObjects", 4.5f);
    }

    private void ShowAllGameObjects()
    {
        foreach (GameObject item in _hideGameObjects)
        {
            item.SetActive(true);
        }

    }
    public void ChangeRigidBodyToGravity()
    {
        foreach (GameObject item in _hideGameObjects)
        {
            item.SetActive(false);
        }



        gameObject.GetComponent<CarMayhem>().ChangeRigidBodyToGravity();
        TriggerButton.interactable = false;
        _mayhemSoundource.Play();

        foreach (Rigidbody rigidbodys in _allchildrenRigidBodys)
        {
            rigidbodys.isKinematic = false;
            rigidbodys.useGravity = true;
        }
        Invoke("BackToStartValues", 10f);

    }


    IEnumerator LerpBackToStartPositionCoroutine()
    {
        float counter = 0f;

        while (counter < _lerpTime)
        {
            for (int i = 0; i < _gameObjectStartsPosition.Count; i++)
            {
                _allchildrenRigidBodys[i].transform.rotation = Quaternion.Lerp(_allchildrenRigidBodys[i].transform.rotation, _gameObjectStartsRotation[i], _speedTime * counter);
                _allchildrenRigidBodys[i].transform.position = Vector3.Lerp(_allchildrenRigidBodys[i].transform.position, _gameObjectStartsPosition[i], _speedTime * counter);
            }
            counter += Time.deltaTime;
            yield return 0f;
        }
    }
}
