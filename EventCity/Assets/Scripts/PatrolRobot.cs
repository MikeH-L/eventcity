﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolRobot : MonoBehaviour
{
    public float _speed = 10f;
    private float _waitTime;
    public float _startWaitTime;

    public Transform[] _waypoints;
    private int _randomWaypoint;

    private void Start()
    {
        _waitTime = _startWaitTime;

        _randomWaypoint = Random.Range(0, _waypoints.Length);
    }


    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _waypoints[_randomWaypoint].position, _speed * Time.deltaTime);

        
            if (Vector3.Distance(transform.position, _waypoints[_randomWaypoint].position) < 0.2f)
        {
            if (_waitTime <= 0)
            {
                _randomWaypoint = Random.Range(0, _waypoints.Length);
                _waitTime = _startWaitTime;
            }

            else
            {
                _waitTime -= Time.deltaTime;
            }
        }
    }
} 
