﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLightFlashing : MonoBehaviour
{
    public Light _redLight;
    public float _duration = 5f;

    private void Start()
    {
        StartCoroutine(fadeInCoroutine(_redLight, _duration));
    }

    IEnumerator fadeInCoroutine(Light lightToFade, float duration)
    {
        float minintensity = 0; 
        float maxintensity = 3; 
        float counter = 0f;


        while (counter < duration)
        {
            counter += Time.deltaTime;

            lightToFade.intensity = Mathf.Lerp(maxintensity, minintensity, counter / duration);

            yield return null;
        }
        StartCoroutine(fadeInCoroutine(_redLight, _duration));
    }
}

