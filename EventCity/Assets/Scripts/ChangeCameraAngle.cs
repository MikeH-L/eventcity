﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCameraAngle : MonoBehaviour
{
    public Transform[] _cameraAngles;
    public Transform _camera;
    private int _locationInArray = 0;


    public void ChangeCityCameraAngle()
    {
        _camera.transform.position = _cameraAngles[_locationInArray].transform.position;
        _camera.transform.rotation = _cameraAngles[_locationInArray].transform.rotation;
        _locationInArray++;
        if (_locationInArray == _cameraAngles.Length)
        {
            _locationInArray = 0;
        }
    }
}
