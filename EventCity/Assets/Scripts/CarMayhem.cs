﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarMayhem : MonoBehaviour
{
    public GameObject _prefabsCarsParent;
    public GameObject _BackToStartValuesButton;

    public Button TriggerButton;
    private Rigidbody[] _allchildrenRigidBodys;

    private List<Vector3> _gameObjectStartsPosition = new List<Vector3>();
    private List<Quaternion> _gameObjectStartsRotation = new List<Quaternion>();

    public AudioSource _mayhemSoundource;
    public AudioSource _rewindSound;
    private float _lerpTime = 4.5f;
    private float _speedTime = 0.02f;

    private void Awake()
    {
        _allchildrenRigidBodys = _prefabsCarsParent.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody go in _allchildrenRigidBodys)
        {
            _gameObjectStartsPosition.Add(go.GetComponent<Transform>().position);
            _gameObjectStartsRotation.Add(go.GetComponent<Transform>().rotation);
        }
    }


    public void BackToStartValues()
    {
        TriggerButton.interactable = true;
        _BackToStartValuesButton.SetActive(false);
        _mayhemSoundource.Stop();

        foreach (Rigidbody rigidbodys in _allchildrenRigidBodys)
        {
            rigidbodys.isKinematic = true;
            rigidbodys.useGravity = false;
        }
        _rewindSound.Play();
        StartCoroutine(LerpBackToStartPositionCoroutine());
    }


    public void ChangeRigidBodyToGravity()
    {
        StartCoroutine(FlickerButtonColorCorroutine());
        TriggerButton.interactable = false;
        _mayhemSoundource.Play();

        foreach (Rigidbody rigidbodys in _allchildrenRigidBodys)
        {
            rigidbodys.isKinematic = false;
            rigidbodys.useGravity = true;
        }
    }


    IEnumerator LerpBackToStartPositionCoroutine()
    {
        float counter = 0f;

        while (counter < _lerpTime)
        {
            for (int i = 0; i < _gameObjectStartsPosition.Count; i++)
            {
                _allchildrenRigidBodys[i].transform.rotation = Quaternion.Lerp(_allchildrenRigidBodys[i].transform.rotation, _gameObjectStartsRotation[i], _speedTime * counter);
                _allchildrenRigidBodys[i].transform.position = Vector3.Lerp(_allchildrenRigidBodys[i].transform.position, _gameObjectStartsPosition[i], _speedTime * counter);
            }
            counter += Time.deltaTime;
            yield return 0f;
        }
    }



    IEnumerator FlickerButtonColorCorroutine()
    {
        float counter = 0f;
        yield return new WaitForSeconds(5f);
        _BackToStartValuesButton.SetActive(true);

        while (true)
        {
            _BackToStartValuesButton.GetComponent<Image>().color = Color.cyan;
            yield return new WaitForSeconds(1f);

            _BackToStartValuesButton.GetComponent<Image>().color = Color.gray;
            yield return new WaitForSeconds(1f);
            counter += Time.deltaTime;
        }
    }
}
