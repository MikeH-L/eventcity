﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickSound : MonoBehaviour
{

    public AudioSource _hoverSound;
    public AudioSource _clickSound;



    public void HoverSoundMethod()
    {

        _hoverSound.Play();
    }

    public void ClickSoundMethod()
    {
        _clickSound.Play();
    }


}
