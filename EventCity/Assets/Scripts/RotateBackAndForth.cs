﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBackAndForth : MonoBehaviour
{
    public float _rotateSpeed = 0.5f;
    private float _counter;
    public float _rotateDistance;

    private Transform _spotLightStartPosition;
    public Transform _SpotLightTransform;




    private void Update()
    {
        if (_counter < _rotateDistance)
        {
            transform.Rotate(Vector3.up * _rotateSpeed * Time.deltaTime);
        }

        if (_counter >= _rotateDistance)
        {
            transform.Rotate(Vector3.up * -_rotateSpeed * Time.deltaTime);

            if (_counter >= _rotateDistance * 2)
            {
                _counter = 0f;
            }
        }
        _counter += Time.deltaTime;
    }
}
