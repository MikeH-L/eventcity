﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportCar : MonoBehaviour
{
    public Vector3 _respawnPosition;
    public GameObject _prefabCar;


    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<ForwardMove>())
        {
            other.transform.position += _respawnPosition;
        }
    }
}
