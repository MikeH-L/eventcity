﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatmanSpotLight : MonoBehaviour
{
    public GameObject _spotlightBatman;
    public bool _batmanSpotLightON;
    public AudioSource _LightSwitchSound;


    private void Start()
    {
        if (_spotlightBatman && _batmanSpotLightON)
        {
            _batmanSpotLightON = true;
            _spotlightBatman.SetActive(true);
        }
    }



    public void ToggleBatmanSpotLight()
    {

        if (_spotlightBatman && _batmanSpotLightON)
        {
            BatmanSpotLightOFF();
        }

        else
        {
            BatmanSpotLightON();
        }
    }

    private void BatmanSpotLightON()
    {
        _LightSwitchSound.Play();
        _spotlightBatman.SetActive(true);
        _batmanSpotLightON = true;
    }

    private void BatmanSpotLightOFF()
    {
        _LightSwitchSound.Play();
        _spotlightBatman.SetActive(false);
        _batmanSpotLightON = false;
    }
}
