﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSkyAtmosphere : MonoBehaviour
{
    public Material[] _skyBoxesMaterial;
    private int _locationInArray = 0;
    

    public void ChangeSkyBox()
    {
        RenderSettings.skybox = _skyBoxesMaterial[_locationInArray];
        _locationInArray++;

        if (_locationInArray == _skyBoxesMaterial.Length)
        {
            _locationInArray = 0;
        }
    }
}
