﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardMove : MonoBehaviour
{
    RaycastHit hit;

    public GameObject _prefab;
    private float _speed = 30f;
    private float __slowerSpeed = 15f;
    private float _range = 30f;
    private float _defaultSpeed;
    
        private void Awake()
        {
        _defaultSpeed = _speed;
        }



    void FixedUpdate()
    {
        // If Raycast detects another spaceship slows the speed down

        if (Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, _range))
        {
            if (hit.transform.GetComponent<ForwardMove>())
            {
                _speed = __slowerSpeed;
            }
        }
        gameObject.transform.Translate(0f, 0f, _speed * Time.deltaTime);

        // If Raycast doesnt detects another spaceship regains the default speed

        if (!Physics.Raycast(gameObject.transform.position, gameObject.transform.forward, out hit, _range))
        {
            _speed = _defaultSpeed;
        }
    }
}
