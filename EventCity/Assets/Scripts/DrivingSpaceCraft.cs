﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrivingSpaceCraft : MonoBehaviour
{
    public float _forwardSpeed = 100f;
    public float _sidewaySpeed = 50f;
    public float _rotateSpeed = 50f;
    public float _gravityForce = 50f;
    public float _liftForce = 100f;
    public Rigidbody _rigidBodyPlayer;
    


    // Update is called once per frame
    void FixedUpdate()
    {
        _rigidBodyPlayer.AddForce(Vector3.down * _gravityForce * Time.deltaTime, ForceMode.Force);

        if (Input.GetKey(KeyCode.W))
        {
            _rigidBodyPlayer.AddRelativeForce(Vector3.forward * _forwardSpeed * Time.deltaTime, ForceMode.Acceleration);
        }

        if (Input.GetKey(KeyCode.S))
        {
            _rigidBodyPlayer.AddRelativeForce(Vector3.back * _forwardSpeed * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.A))
        {
            _rigidBodyPlayer.AddRelativeForce(Vector3.left * _sidewaySpeed * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.D))
        {
            _rigidBodyPlayer.AddRelativeForce(Vector3.right * _sidewaySpeed * Time.deltaTime, ForceMode.Force);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            _rigidBodyPlayer.AddForce(Vector3.up * _liftForce * Time.deltaTime, ForceMode.Acceleration);
        }



        if (Input.GetKey(KeyCode.Q))
        {
            _rigidBodyPlayer.transform.Rotate(Vector3.up * -_rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            _rigidBodyPlayer.transform.Rotate(Vector3.up * _rotateSpeed * Time.deltaTime);
        }
    }
}
