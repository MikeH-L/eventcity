﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LightningStorm : MonoBehaviour
{
    public GameObject _flashLight;
    public GameObject _prefabLightningParticle;

    private Transform _lightningTransformPositionStart;

    public AudioSource _lightningStrikeSound;

    public Button _toggleLightningStormButton;

    public float _lightstormRadiusArea = 300f;
    public float _nextLightningSpawnTime = 3f;

    private Vector3 _startPosition;
    private Quaternion _startRotation;
    private Color _startColorButton;
    private Color _triggerOnColor = new Color(255, 0, 0);
    private bool _lightningstormON;

    private void Start()
    {
        _startPosition = transform.position;
        _startRotation = transform.rotation;
        _startColorButton = _toggleLightningStormButton.GetComponent<Image>().color;
    }


    public void TurnOnLightningStormParticle()
    {
        if (!_lightningstormON && GetComponent<ToggleLights>()._lightsOn)
        {
            TurnLightningStormON();
        }

        else
        {
            TurnLightningStormOFF();
        }
    }

    void TurnLightningStormON()
    {
        _toggleLightningStormButton.GetComponent<Image>().color = _triggerOnColor;
        StartCoroutine(LightningStormCorroutine());
    }

    void TurnLightningStormOFF()
    {
        _toggleLightningStormButton.GetComponent<Image>().color = _startColorButton;
        _lightningstormON = false;
    }



    private IEnumerator LightningStormCorroutine()
    {
        _lightningstormON = true;
        float counter = 0f;
        Vector3 newRandomPosition = new Vector3(Random.Range(-_lightstormRadiusArea, _lightstormRadiusArea), 0f, (Random.Range(-_lightstormRadiusArea, _lightstormRadiusArea)));
        _flashLight.SetActive(true);
        _lightningStrikeSound.pitch = Random.Range(0f,1.9f);
        _lightningStrikeSound.Play();
        GameObject _newLightningStrike = Instantiate(_prefabLightningParticle, newRandomPosition, _startRotation);

        if (!_lightningstormON)
        {
            Destroy(_newLightningStrike);
        }

        while (_lightningstormON && counter <= _nextLightningSpawnTime)
        {

            if  (counter >= 0.3)
            {
                    Destroy(_newLightningStrike.GetComponentInChildren<Light>());
            }


            counter += Time.deltaTime;
        yield return null;
        }

        _flashLight.SetActive(false);

        if (!_lightningstormON)
        {
            Destroy(_newLightningStrike);
        }

        if (_lightningstormON)
        {
            Destroy(_newLightningStrike);
            StartCoroutine(LightningStormCorroutine());
        }
    }
}
