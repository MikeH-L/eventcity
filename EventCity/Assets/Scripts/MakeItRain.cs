﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeItRain : MonoBehaviour
{
    public AudioSource _rainSound;
    public GameObject _particleRain;
    public bool _itRains;

    private void Awake()
    {
        if (_itRains)
        {
            _particleRain.SetActive(true);
        }
    }


    public void ToggleRain()
    {


        
        if (!_itRains && _particleRain)
        {
        Rain();
        }

        

        else
        {
            StopTheRain();
        }
    }


    public void Rain()
    {
        if (!_itRains)
        {
            _rainSound.Play();
            _particleRain.SetActive(true);
            _itRains = true;
        }
    }


    public void StopTheRain()
    {
        if (_itRains)
        {
            _rainSound.Stop();
            _particleRain.SetActive(false);
            _itRains = false;
        }
    }
}
