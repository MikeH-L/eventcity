﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleLights : MonoBehaviour
{
    public GameObject[] _allLightParentObjects;
    public GameObject _flashLight;
    public GameObject _prefabLightningParticle;
    private Transform _lightningTransformPositionStart;

    public Button _toggleLightButton;

    public AudioSource _lightningStrikeSound;
    public AudioSource _powerOutageSound;
    public AudioSource _powerBackOnSound;

    private Color _triggerOnColor = new Color(255, 0, 0);

    public bool _lightsOn = true;
    private float _startPitchLightningSound;
    private Color _startColorButton = new Color();

    private void Awake()
    {
        _startPitchLightningSound = _lightningStrikeSound.pitch;
        _startColorButton = _toggleLightButton.GetComponent<Image>().color;
        if (!_lightsOn)
        {
            SetAllWindowsObjectInActive();
        }

    }


    [ContextMenu("Toggle City Lights")]
    public void ToggleCityLights()
    {
        if (!_lightsOn)
        {
            _powerBackOnSound.Play();
            _toggleLightButton.interactable = false;
            StartCoroutine(FlickerLightCoroutine());
            Invoke("TurnOnLights",1f);
            _lightsOn = true;
        }

        else 
        {
            TurnOffLights();
            _lightsOn = false;
        }
    }


    public void TurnOnLights()
    {
        _flashLight.SetActive(false);
        _toggleLightButton.interactable = true;
        _toggleLightButton.GetComponent<Image>().color = _startColorButton;
    }

    public void TurnOffLights()
    {
        _flashLight.SetActive(true);
        _powerOutageSound.Play();
        _toggleLightButton.interactable = false;
        _toggleLightButton.GetComponent<Image>().color = _triggerOnColor;
        _lightningStrikeSound.pitch = _startPitchLightningSound;
        _lightningStrikeSound.Play();
        GameObject newSpawnLightning = Instantiate(_prefabLightningParticle);
        Destroy(newSpawnLightning.GetComponentInChildren<Light>(), 0.3f);
        Invoke("SetAllWindowsObjectInActive", 0.5f);
    }

    void SetAllWindowsObjectInActive()
    {
        _toggleLightButton.interactable = true;


        foreach (GameObject go in _allLightParentObjects)
        {
            go.SetActive(false);
        }
    }


    IEnumerator FlickerLightCoroutine()
    {
        float counter = 0f;

        while (counter < 0.3f)
        {
            foreach (GameObject go in _allLightParentObjects)
            {
                go.SetActive(false);
            }
            yield return new WaitForSeconds(Random.Range(0f, 0.1f));

            foreach (GameObject go in _allLightParentObjects)
            {
                go.SetActive(true);
            }
            yield return new WaitForSeconds(Random.Range(0f, 0.1f));

            counter += Time.deltaTime;
        }
    }
}


